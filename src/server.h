#ifndef SERVER_H
#define SERVER_H

#include <QObject>

#include <QWebdav>

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString displayName READ displayName WRITE setDisplayName NOTIFY displayNameChanged)
    Q_PROPERTY(ConnectionType connectionType READ connectionType WRITE setConnectionType NOTIFY connectionTypeChanged)
    Q_PROPERTY(QString hostname READ hostname WRITE setHostname NOTIFY hostnameChanged)
    Q_PROPERTY(QString rootPath READ rootPath WRITE setRootPath NOTIFY rootPathChanged)
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QString sslCertDigestMd5 READ sslCertDigestMd5 WRITE setSslCertDigestMd5 NOTIFY sslCertDigestMd5Changed)
    Q_PROPERTY(QString sslCertDigestSha1 READ sslCertDigestSha1 WRITE setSslCertDigestSha1 NOTIFY sslCertDigestSha1Changed)

public:
    enum ConnectionType { HTTP = QWebdav::HTTP, HTTPS = QWebdav::HTTPS };

    Q_ENUM(ConnectionType);

    explicit Server(QObject* parent = 0);
    Server(const Server& other);
    Server& operator=(const Server& other);

    QString displayName() const;
    void setDisplayName(const QString &name);
    ConnectionType connectionType() const;
    void setConnectionType(const ConnectionType connectionType = HTTPS);
    QString hostname() const;
    void setHostname(const QString &hostname);
    QString rootPath() const;
    void setRootPath(const QString &rootPath);
    QString username() const;
    void setUsername(const QString &username);
    QString password() const;
    void setPassword(const QString &password);
    int port() const;
    void setPort(const int port);
    QString sslCertDigestMd5() const;
    void setSslCertDigestMd5(const QString &sslCert);
    QString sslCertDigestSha1() const;
    void setSslCertDigestSha1(const QString &sslCert);

Q_SIGNALS:
    void displayNameChanged();
    void connectionTypeChanged();
    void hostnameChanged();
    void rootPathChanged();
    void usernameChanged();
    void passwordChanged();
    void portChanged();
    void sslCertDigestMd5Changed();
    void sslCertDigestSha1Changed();

private:
    QString m_displayName;
    ConnectionType m_connectionType;
    QString m_hostname;
    QString m_rootPath;
    QString m_username;
    QString m_password;
    int m_port;
    QString m_sslCertDigestMd5;
    QString m_sslCertDigestSha1;


};
Q_DECLARE_METATYPE(Server)
#endif // SERVER_H
