#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QDebug>

#include "client.h"
#include "server.h"
#include "settingshelper.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<Client>("org.kde.webdavapp", 1, 0, "Client");
    qmlRegisterType<Server>("org.kde.webdavapp", 1, 0, "Server");
    qmlRegisterType<SettingsHelper>("org.kde.webdavapp", 1, 0, "SettingsHelper");
    qmlRegisterInterface<Server>("Server");
    QGuiApplication app(argc, argv);

    QCoreApplication::setApplicationName("webdavapp");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
