import QtQuick 2.5
import org.kde.kirigami 2.0 as Kirigami

import org.kde.webdavapp 1.0

Kirigami.Page {
    title: Qt.application.name
    Column {
        anchors.fill: parent

        Item {
            id: content
            Text {
                text: "This is the initialization page, where we explain stuff about the app."
            }
        }
    }
}
