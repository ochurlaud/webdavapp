import QtQuick 2.5
import QtQuick.Controls 2.0
import org.kde.kirigami 2.0 as Kirigami
import org.kde.okular 2.0
import org.kde.webdavapp 1.0

Kirigami.ApplicationWindow {
    id: root

    ServerSettingsPage {
        id: serverSettingsPage
    }

    SettingsHelper {
        id: settingshelper
    }

    globalDrawer: GlobalDrawer {
        id: globalDrawer
    }

    header: Kirigami.ApplicationHeader {
        headerStyle: Kirigami.Breadcrump
    }
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
        Kirigami.ScrollablePage {
            title: "Glob"
            Rectangle {
                anchors.fill: parent
                Text {
                    text: "In Context"
                }
            }
        }
    }

    pageStack.initialPage: InitPage { }
    ShowDirectoriesPage {
        id: showDirectoriesPage
    }

    function showEditPage(serverName) {
        serverSettingsPage.serverName = serverName
        serverSettingsPage.updatePage()
        pageStack.push(serverSettingsPage);
    }

    function showMainPage(serverName) {
        pageStack.push(showDirectoriesPage)
        showDirectoriesPage.serverName = serverName
        showDirectoriesPage.updatePage(serverName)
    }

    function serverAdded() {
        globalDrawer.updateModel()
        showMainPage()
    }
    Component.onCompleted: {
        globalDrawer.editRequested.connect(showEditPage)
        globalDrawer.showDirectoriesRequested.connect(showMainPage)
        serverSettingsPage.submitted.connect(serverAdded)
    }
}
