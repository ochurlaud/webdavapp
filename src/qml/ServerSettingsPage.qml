import QtQuick 2.5
import QtQuick.Controls 2
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.0 as Kirigami

import org.kde.webdavapp 1.0


Kirigami.Page {
    id: serverSettingsPage

    function updatePage() {
        updateForm(serverSettingsPage)
    }
    property alias serverName: displayName.text
    property alias displayName: displayName.text
    property alias protocolHttp: protocolHttp.checked
    property alias protocolHttps: protocolHttps.checked
    property alias hostname: hostname.text
    property alias port: port.value
    property alias username: username.text
    property alias password: password.text
    property alias rootPath: rootPath.text
    property alias sslCertDigestMd5: sslCertDigestMd5.text
    property alias sslCertDigestSha1: sslCertDigestSha1.text

    signal submitted(string serverName)

    title: "Setup your server"
    contentItem: Item {
        id: contentItem
        height: 200
        Column {
            GridLayout {
                columns: 2

                Label {
                    text: "Display Name"
                }
                TextField {
                    id: displayName
                    text: serverName
                }
                Label {
                    text: "Protocol"
                }
                GroupBox {
                    id: protocol
                    padding: 0
                    background: Item {}
                    Row {
                        RadioButton {
                            text: "http://"
                            id: protocolHttp
                        }
                        RadioButton {
                            text: "https://"
                            id: protocolHttps
                        }
                    }
                }
                Label {
                    text: "Hostname"
                }
                TextField {
                    id: hostname
                }
                Label {
                    text: "Port"
                }
                SpinBox {
                    id: port
                    editable: true
                    value: (protocolHttp.checked ? 80 : 443)
                    from: 0
                    to: 65535
                }
                Label {
                    text: "Username"
                }
                TextField {
                    id: username
                }
                Label {
                    text: "Password"
                }
                TextField {
                    id: password
                    echoMode: TextInput.Password
                }
                Label {
                    text: "Root path"
                }
                TextField {
                    id: rootPath
                }
                Label {
                    text: "sslCertDigestMd5"
                }
                TextField {
                    id: sslCertDigestMd5
                }
                Label {
                    text: "sslCertDigestSha1"
                }
                TextField {
                    id: sslCertDigestSha1
                }
                Component.onCompleted: updateForm(serverSettingsPage)
            }
            Button {
                text: "Submit"
                onClicked: {
                    updateObject(serverSettingsPage);
                    serverSettingsPage.submited(serverSettingsPage.serverName);
                }
            }
        }
    }
    function updateForm(obj) {
        var server = serverComponent.createObject(serverSettingsPage)

        if (obj.displayName != "") {
            server = settingshelper.loadServer(obj.displayName, serverSettingsPage);
        } else {

        }
        obj.displayName = server.displayName
        obj.hostname = server.hostname
        obj.port = server.port
        if (server.connectionType == Server.HTTP) {
            obj.protocolHttp = true
        } else {
            obj.protocolHttps = true
        }
        obj.username = server.username
        obj.password = server.password
        obj.rootPath = server.rootPath
        obj.sslCertDigestMd5 = server.sslCertDigestMd5
        obj.sslCertDigestSha1 = server.sslCertDigestSha1
    }

    function updateObject(obj) {
        var server = serverComponent.createObject(serverSettingsPage)
        server.displayName = obj.displayName
        server.hostname = obj.hostname
        server.port = obj.port
        if (obj.protocolHttp == true) {
            server.connectionType = Server.HTTP
        } else {
            server.connectionType = Server.HTTPS
        }
        server.username = obj.username
        server.password = obj.password
        server.rootPath = obj.rootPath
        server.sslCertDigestMd5 = obj.sslCertDigestMd5
        server.sslCertDigestSha1 = obj.sslCertDigestSha1
        settingshelper.saveServer(server)
    }
    SettingsHelper {
        id: settingshelper
    }
    Component {
        id: serverComponent

        Server {
            id: server
        }
    }
}
