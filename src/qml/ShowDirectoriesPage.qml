import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4 as OldControl

import org.kde.kirigami 2.0 as Kirigami

import org.kde.webdavapp 1.0

Kirigami.Page {
    id: root
    title: serverName
    property string serverName
    function updatePage(serverName) {
        var server = settingshelper.loadServer(serverName, root)
        client.setServer(server)
       // client.getFile("/sync.sh")
        client.showDirectories()
    }
    Column {
        anchors.fill: parent

        Item {
            id: treeview
            width: parent.width
            height: parent.height - 50
            OldControl.TreeView {

               OldControl.TableViewColumn {
                    title: "Name"
                    role: "display"
                    width: 300
                }
                anchors.fill: parent
                model: client.fileModel

                itemDelegate: CheckBox {
                    id: control
                    tristate: true
                    text: styleData.value
                    checked: true
                    indicator.implicitWidth: 15
                    indicator.implicitHeight: 15
                }
            }
        }
    }

    Client {
        id: client
    }

    SettingsHelper {
        id: settingshelper
    }
}
