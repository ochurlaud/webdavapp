import QtQuick 2.5
import QtQuick.Controls 2.0

import org.kde.kirigami 2.0 as Kirigami

Kirigami.GlobalDrawer {
    id: globalDrawer
    title: "Servers"
    titleIcon: "applications-graphics"
    handleVisible: true

    signal editRequested(string serverName)
    signal showDirectoriesRequested(string serverName)
    function updateModel() {
        serverListView.model = settingshelper.getServerList().sort()
    }

    ListView {
        id: serverListView
        anchors.top: globalDrawer.top
        height: 250

        model: settingshelper.getServerList().sort()
        delegate: Kirigami.SwipeListItem {
            width: globalDrawer.width
            height: 40
            actions: [
                Kirigami.Action {
                    iconName: "entry-delete"
                    onTriggered: {
                        settingshelper.removeServer(modelData)
                        globalDrawer.updateModel()
                    }
                },
                Kirigami.Action {
                    iconName: "entry-edit"
                    onTriggered: {
                        globalDrawer.editRequested(modelData)
                    }
                }
            ]
            Label {
                text: modelData

                MouseArea {
                    hoverEnabled: true
                    anchors.fill: parent
                    onClicked: {
                        showDirectoriesRequested(modelData)
                    }
                }
            }

        }
    }
    Row {
        Item {
            height: 50
            width: 50
            Kirigami.Icon {
                source: "view-refresh"
                anchors.fill: parent
            }
            MouseArea {
                anchors.fill: parent
                //onClicked: pageStack.push(addServerComponent);
            }
        }
        Item {
            height: 50
            width: 50
            Kirigami.Icon {
                source: "list-add"
                anchors.fill: parent
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    globalDrawer.editRequested("")
                }
            }
        }
    }
}
