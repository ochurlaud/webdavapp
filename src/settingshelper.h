#ifndef SETTINGSHELPER_H
#define SETTINGSHELPER_H

#include <QObject>

#include "server.h"

class SettingsHelper: public QObject
{
    Q_OBJECT

public:
    explicit SettingsHelper(QObject* parent = 0);
    //Q_INVOKABLE static void loadServer(Server* server, const QString& name);
    Q_INVOKABLE static Server* loadServer(const QString& name, QObject* parent);
    Q_INVOKABLE static void saveServer(Server* server);
    Q_INVOKABLE static void removeServer(const QString& name);
    Q_INVOKABLE static QStringList getServerList();
};

#endif // SETTINGSHELPER_H
