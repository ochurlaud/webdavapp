#include "server.h"

Server::Server(QObject *parent)
    : QObject(parent)
{
}

Server::Server(const Server& other)
    : QObject(other.parent())
{
    *this = other;
}

Server& Server::operator=(const Server& other)
{
    this->setParent(other.parent());
    m_displayName = other.displayName();
    m_hostname = other.hostname();
    m_port = other.port();
    m_connectionType = other.connectionType();
    m_username = other.username();
    m_password = other.password();
    m_sslCertDigestMd5 = other.sslCertDigestMd5();
    m_sslCertDigestSha1 = other.sslCertDigestSha1();
    m_rootPath = other.rootPath();
}

QString Server::displayName() const
{
    return m_displayName;
}

void Server::setDisplayName(const QString &name)
{
    m_displayName = name;
}

Server::ConnectionType Server::connectionType() const
{
    return m_connectionType;
}

void Server::setConnectionType(const ConnectionType connectionType)
{
    m_connectionType = connectionType;
}

QString Server::hostname() const
{
    return m_hostname;
}

void Server::setHostname(const QString &hostname)
{
    m_hostname = hostname;
}

QString Server::rootPath() const
{
    return m_rootPath;
}

void Server::setRootPath(const QString &rootPath)
{
    m_rootPath = rootPath;
}

QString Server::username() const
{
    return m_username;
}

void Server::setUsername(const QString &username)
{
    m_username = username;
}

QString Server::password() const
{
    return m_password;
}

void Server::setPassword(const QString &password)
{
    m_password = password;
}

int Server::port() const
{
    return m_port;
}

void Server::setPort(const int port)
{
    m_port = port;
}

QString Server::sslCertDigestMd5() const
{
    return m_sslCertDigestMd5;
}

void Server::setSslCertDigestMd5(const QString &sslCert)
{
    m_sslCertDigestMd5 = sslCert;
}

QString Server::sslCertDigestSha1() const
{
    return m_sslCertDigestSha1;
}

void Server::setSslCertDigestSha1(const QString &sslCert)
{
    m_sslCertDigestSha1 = sslCert;
}
