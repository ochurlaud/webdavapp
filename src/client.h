#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QHash>
#include <QStandardItemModel>

#include <QWebdav>
#include <QWebdavDirParser>
#include <QWebdavItem>

#include "server.h"

class QNetworkReply;
class QStandardItem;

class Client : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStandardItemModel* fileModel READ fileModel NOTIFY fileModelChanged)

public:
    explicit Client(QObject* parent = 0);
    void setServer(const QWebdav::QWebdavConnectionType &connectionType,
                   const QString &hostname = "",
                   const QString &rootPath = "",
                   const QString &username = "",
                   const QString &password = "",
                   const int port = 80,
                   const QString &sslCertDigestMd5 = "",
                   const QString &sslCertDigestSha1 = "");
    Q_INVOKABLE void setServer(Server* server);
    Q_INVOKABLE void showDirectories(const QString &path = "/", bool recursive = false);
    Q_INVOKABLE void getFile(const QString& sourcePath, const QString &targetPath);
    Q_INVOKABLE void getAllFiles();
    QStandardItemModel* fileModel() { return m_fileModel; };

Q_SIGNALS:
    void printDone();
    void fileModelChanged();

public Q_SLOTS:
    void printList();
    void printError(QString errorMsg);
    void replySkipRead();

private:
    int m_nbError;
    int m_nbErrorMax;
    QWebdav m_server;
    QWebdavDirParser m_dirParser;
    QString m_path;
    QList<QNetworkReply *> m_replyList;
    QStandardItemModel* m_fileModel;
    QHash<QString, QStandardItem*> m_nameToItem;
    QStringList m_queryList;
    bool m_printBlock;
};

#endif // CLIENT_H
