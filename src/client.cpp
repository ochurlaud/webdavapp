#include "client.h"

Client::Client(QObject *parent)
    : QObject(parent)
    , m_fileModel(nullptr)
{
    connect(&m_dirParser, &QWebdavDirParser::errorChanged, this, &Client::printError);
    connect(&m_server, &QWebdav::errorChanged, this, &Client::printError);

    m_nbErrorMax = 3;
    m_nbError = 0;
    m_fileModel = new QStandardItemModel(0, 0);
    QStandardItem *item = new QStandardItem(QString("/"));
    m_fileModel->appendRow(item);
    m_nameToItem["/"] = item;
    m_queryList << "/";
    connect(this, &Client::printDone,
            [=]() { m_nbError = 0; m_printBlock = false; this->showDirectories();}
           );
}

void Client::setServer(const QWebdav::QWebdavConnectionType &connectionType,
                       const QString &hostname,
                       const QString &rootPath,
                       const QString &username,
                       const QString &password,
                       const int port,
                       const QString &sslCertDigestMd5,
                       const QString &sslCertDigestSha1)
{
    m_server.setConnectionSettings(connectionType, hostname, rootPath, username, password, port, sslCertDigestMd5, sslCertDigestSha1);
}

void Client::setServer(Server* server)
{
    this->setServer(static_cast<QWebdav::QWebdavConnectionType>(server->connectionType()),
                    server->hostname(),
                    server->rootPath(),
                    server->username(),
                    server->password(),
                    server->port(),
                    server->sslCertDigestMd5(),
                    server->sslCertDigestSha1());
}

void Client::showDirectories(const QString &path, bool recursive)
{
    if (!m_queryList.isEmpty() && !m_printBlock) {
        m_printBlock = true;
        m_path = m_queryList.takeFirst();
        if (m_path != "/") {
            m_path += '/';
        }
        qDebug() << "search path" << m_path;
        m_dirParser.listDirectory(&m_server, m_path);
        connect(&m_dirParser, &QWebdavDirParser::finished, this, &Client::printList);
    }
}

void Client::printList()
{
    disconnect(&m_dirParser, &QWebdavDirParser::finished, this, &Client::printList);
    QList<QWebdavItem> list = m_dirParser.getList();

    QWebdavItem webdavItem;
    foreach(webdavItem, list) {
        QString name = m_path + webdavItem.name();
        QStandardItem *item = new QStandardItem(webdavItem.name());
        QString correctedPath = m_path;
        if (m_path != "/") {
            correctedPath.remove(m_path.count()-1, 1);
        }

        QStandardItem *parent = m_nameToItem[correctedPath];
        parent->appendRow(item);
        m_nameToItem[name] = item;

        if (webdavItem.isDir()) {
            m_queryList << m_path + webdavItem.name();
            qDebug() << "New dir found:" << webdavItem.name();
        }

       // QNetworkReply *reply = m_server.get(item.path());
       // connect(reply, SIGNAL(readyRead()), this, SLOT(replySkipRead()));
       // m_replyList.append(reply);
    }
    emit printDone();
}

void Client::printError(QString errorMsg)
{
    qDebug() << "QWebdav::printErrors()  errorMsg == " << errorMsg;

    m_nbError++;
    if (m_nbError < m_nbErrorMax) {
        qDebug() << "Let's retry" << m_nbError;
        QString correctedPath = m_path;
        correctedPath.remove(m_path.count()-1, 1);
        m_queryList.prepend(correctedPath);
    }
}

void Client::replySkipRead()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(QObject::sender());
    if (reply==0)
        return;

    QByteArray ba = reply->readAll();

    qDebug() << "QWebdav::replySkipRead()   skipped " << ba.size() << " reply->url() == " << reply->url().toString(QUrl::RemoveUserInfo);
}

void Client::getFile(const QString& sourcePath, const QString &targetPath)
{
    QFile* file = new QFile(targetPath, this);
    file->open(QIODevice::WriteOnly);
    QNetworkReply* reply = m_server.get(sourcePath, file);
}

void Client::getAllFiles()
{
    foreach(const QString& name, m_nameToItem.keys()) {
        QString targetPath(name);
        targetPath.remove(0, 1);
        this->getFile(name, targetPath);
    }
}
