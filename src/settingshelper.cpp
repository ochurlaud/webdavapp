#include "settingshelper.h"

SettingsHelper::SettingsHelper(QObject* parent)
    : QObject(parent)
{}

Server* SettingsHelper::loadServer(const QString& name, QObject *parent)
{
    Server *server = new Server(parent);
    QSettings settings(qApp->applicationName(), QStringLiteral("servers"));

    server->setDisplayName(name);
    server->setHostname(settings.value(name + QStringLiteral("/hostname")).toString());
    server->setPort(settings.value(name + QStringLiteral("/port")).toInt());
    server->setConnectionType(
        static_cast<Server::ConnectionType>(settings.value(name + QStringLiteral("/connectionType")).toInt())
    );
    server->setUsername(settings.value(name + QStringLiteral("/username")).toString());
    server->setPassword(QByteArray::fromBase64(settings.value(name + QStringLiteral("/password")).toByteArray()));
    server->setRootPath(settings.value(name + QStringLiteral("/rootPath")).toString());
    server->setSslCertDigestMd5(settings.value(name + QStringLiteral("/sslCertDigestMd5")).toString());
    server->setSslCertDigestSha1(settings.value(name + QStringLiteral("/sslCertDigestSha1")).toString());
    return server;
}

void SettingsHelper::saveServer(Server* server)
{
    QSettings settings(qApp->applicationName(), QStringLiteral("servers"));
    QString name = server->displayName();
    settings.setValue(name + QStringLiteral("/hostname"), server->hostname());
    settings.setValue(name + QStringLiteral("/port"), server->port());
    settings.setValue(name + QStringLiteral("/connectionType"),
        static_cast<int>(server->connectionType())
    );
    settings.setValue(name + QStringLiteral("/username"), server->username());
    settings.setValue(name + QStringLiteral("/password"), QString(server->password().toUtf8().toBase64()));
    settings.setValue(name + QStringLiteral("/rootPath"), server->rootPath());
    settings.setValue(name + QStringLiteral("/sslCertDigestMd5"), server->sslCertDigestMd5());
    settings.setValue(name + QStringLiteral("/sslCertDigestSha1"), server->sslCertDigestSha1());
}

void SettingsHelper::removeServer(const QString& name)
{
    QSettings settings(qApp->applicationName(), QStringLiteral("servers"));
    settings.remove(name);
}

QStringList SettingsHelper::getServerList()
{
    QSettings settings(qApp->applicationName(), QStringLiteral("servers"));
    return settings.childGroups();
}
